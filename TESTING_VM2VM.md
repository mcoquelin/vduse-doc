# Testing VM to VM scenario

This test consists in creating two VDUSE devices and add them to an OVS brisge.<br>
The VDUSE devices are bound to vhost-vdpa driver.<br>
Doing that, we get two vhost-vdpa chardevs that we pass to two virtual machines that can then ping each other.

Tuning of the system to get the best performance is covered in the end.

## Insert VDUSE and Virtio-vDPA modules dynamically

Either probe the required modules dynamically:

``` bash
sudo modprobe vduse
sudo modprobe virtio_vdpa
sudo modprobe vhost_vdpa
```

Or set it persistent across reboots:

``` bash
cat > /etc/modules-load.d/vduse.conf << EOL
vduse
virtio_vdpa
vhost_vdpa
EOL
```

## Start OVS with DPDK enabled

Currently, using VDUSE in OVS-DPDK requires root permissions.

``` bash
sudo su
export PATH=$PATH:/usr/local/bin:/usr/local/share/openvswitch/scripts
ovs-ctl --no-ovs-vswitchd start
ovs-vsctl --no-wait set Open_vSwitch . other_config:dpdk-init="true"
ovs-vsctl --no-wait set Open_vSwitch . other_config:userspace-tso-enable="true"
ovs-ctl --no-ovsdb-server start
```

## Create bridge and VDUSE devices

We create vduse0 and vduse1 ports. <br>
The distinction between vhost-user and vduse ports is done automatically by the Vhost library based on the path (/dev/vduse/).


``` bash
ovs-vsctl add-br br0 -- set bridge br0 datapath_type=netdev
ovs-vsctl add-port br0 vduse0 -- set Interface vduse0 type=dpdkvhostuserclient options:vhost-server-path=/dev/vduse/vduse0
ovs-vsctl add-port br0 vduse1 -- set Interface vduse1 type=dpdkvhostuserclient options:vhost-server-path=/dev/vduse/vduse1
```

We can check the VDUSE devices are created:

``` bash
ls -l /dev/vduse/
```
``` log
total 0
crw-------. 1 root root 235, 0 Jul 26 11:44 control
crw-------. 1 root root 235, 1 Jul 26 11:46 vduse0
crw-------. 1 root root 235, 2 Jul 26 11:46 vduse1
```

## Attach the VDUSE devices to the vDPA bus

The VDUSE devices are created, but they are not attached yet to the vDPA bus.<br>
This is done via a Netlink socket. Iproute2 provide a vdpa tool to help with that:

``` bash
vdpa dev add name vduse0 mgmtdev vduse
driverctl -b vdpa set-override vduse0 vhost_vdpa
vdpa dev add name vduse1 mgmtdev vduse
driverctl -b vdpa set-override vduse1 vhost_vdpa
```
## Install the virtual machines

### Install QEMU/KVM, libvirt and tools

We start with installing the virtualization group and other dependencies:

```bash
dnf install -y vim @virtualization guestfs-tools
```

Libvirtd service may need to be started and enabled:

``` bash
systemctl start libvirtd
systemctl enable libvirtd
```

### Create and configure the VMs

We download the cloud image, here Fedora 41:
```bash
curl -JL https://download.fedoraproject.org/pub/fedora/linux/releases/41/Cloud/x86_64/images/Fedora-Cloud-Base-Generic-41-1.4.x86_64.qcow2 -o /var/lib/libvirt/images/Fedora-Cloud-Base-Generic-41-1.4.x86_64.qcow2
```

Then, create two disk images for each VMs based on 

```bash
qemu-img create -f qcow2 -F qcow2 -b /var/lib/libvirt/images/Fedora-Cloud-Base-Generic-41-1.4.x86_64.qcow2 /var/lib/libvirt/images/fc41-1.qcow2 20G
qemu-img create -f qcow2 -F qcow2 -b /var/lib/libvirt/images/Fedora-Cloud-Base-Generic-41-1.4.x86_64.qcow2 /var/lib/libvirt/images/fc41-2.qcow2 20G
```

If not done already, generate a SSH key on the host to connect to the guests via SSH:

```bash
ssh-keygen -t rsa -b 4096
```

We prepare the created images, which include removing the cloud-init, set the default root password and install SSH key:

```bash
virt-sysprep --root-password password:changeme --uninstall cloud-init --selinux-relabel -a /var/lib/libvirt/images/fc41-1.qcow2 --ssh-inject root:file:/root/.ssh/id_rsa.pub
virt-sysprep --root-password password:changeme --uninstall cloud-init --selinux-relabel -a /var/lib/libvirt/images/fc41-2.qcow2 --ssh-inject root:file:/root/.ssh/id_rsa.pub
```

We can now install create the guest into libvirt:

```bash
virt-install --import  --name fc41-1 --ram=4096 --vcpus=3 \
    --nographics --accelerate \
    --network network:default,model=virtio --mac 02:ca:fe:fa:ce:aa \
    --debug --wait 0 --console pty \
    --disk /var/lib/libvirt/images/fc41-1.qcow2,bus=virtio \
    --os-variant fedora40
virt-install --import  --name fc41-2 --ram=4096 --vcpus=3 \
    --nographics --accelerate \
    --network network:default,model=virtio --mac 02:ca:fe:fa:ce:ab \
    --debug --wait 0 --console pty \
    --disk /var/lib/libvirt/images/fc41-2.qcow2,bus=virtio \
    --os-variant fedora40
```

If everything went as expected, we now have two guest running:

```bash
virsh list
 Id   Name     State
------------------------
 1    fc41-1   running
 2    fc41-2   running
```

We can stop them to configure their XML files:

```bash
virsh shutdown fc41-1
virsh shutdown fc41-2
```

First, we configure the guests to use locked hugepages:

```bash
virt-xml fc41-1 --edit --memorybacking access.mode=shared
virt-xml fc41-2 --edit --memorybacking access.mode=shared
```

And we add the vDPA device, that will connect to the VDUSE devices we previously created via Vhost-vDPA.
To achieve this, we edit the XML directly:
```bash
virsh edit fc41-1
```

Then we add below XML snippet to connect to vduse0 device:
```xml
<devices>
  <interface type='vdpa'>
    <source dev='/dev/vhost-vdpa-0'/>
  </interface>
</devices>
```

Save and quit, then do the same on the other guest to connect vduse1:

```bash
virsh edit fc41-2
```
```xml
<devices>
  <interface type='vdpa'>
    <source dev='/dev/vhost-vdpa-1'/>
  </interface>
</devices>
```

Now the VMs can be started:

```bash
virsh start fc41-1
virsh start fc41-2
```

Once started, we can retrieve the IP addressed on the primary interfaces of the guests (not the VDUSE ones):
```bash
virsh net-dhcp-leases default
 Expiry Time           MAC address         Protocol   IP address           Hostname   Client ID or DUID
------------------------------------------------------------------------------------------------------------
 2025-01-24 04:47:43   02:ca:fe:fa:ce:aa   ipv4       192.168.122.210/24   -          01:02:ca:fe:fa:ce:aa
 2025-01-24 04:47:45   02:ca:fe:fa:ce:ab   ipv4       192.168.122.211/24   -          01:02:ca:fe:fa:ce:ab
```

Based on the MAC addresses, fc41-1 has 192.168.122.210 and fc41-2 192.168.122.211.
We can add them to the hosts file to ease later connections via SSH:

```bash
echo "192.168.122.210 fc41-1" >> /etc/hosts
echo "192.168.122.211 fc41-2" >> /etc/hosts
```

## Basic connectivity testing

We manually set IP addresses to each Virtio interfaces (the VDUSE ones) in the guests.
Let's start wit retrieving the interface names:

 ```bash
ssh fc41-1 ip l
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 02:ca:fe:fa:ce:aa brd ff:ff:ff:ff:ff:ff
3: enp7s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:d5:e5:16 brd ff:ff:ff:ff:ff:ff
```

```bash
ssh fc41-2 ip l
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 02:ca:fe:fa:ce:ab brd ff:ff:ff:ff:ff:ff
3: enp7s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:68:de:bd brd ff:ff:ff:ff:ff:ff
```

Interfaces are named "enp7s0" in both guests.

```bash
ssh fc41-1 "nmcli dev set enp7s0 managed no;ip a a dev enp7s0 192.168.101.1/24"
ssh fc41-2 "nmcli dev set enp7s0 managed no;ip a a dev enp7s0 192.168.101.2/24"
```
If everything went well, it is now possible for guests to ping each other via their VDUSE interfaces:

```bash
ssh fc41-1 ping 192.168.101.2
PING 192.168.101.2 (192.168.101.2) 56(84) bytes of data.
64 bytes from 192.168.101.2: icmp_seq=1 ttl=64 time=0.561 ms
64 bytes from 192.168.101.2: icmp_seq=2 ttl=64 time=0.347 ms
64 bytes from 192.168.101.2: icmp_seq=3 ttl=64 time=0.114 ms
64 bytes from 192.168.101.2: icmp_seq=4 ttl=64 time=0.114 ms
64 bytes from 192.168.101.2: icmp_seq=5 ttl=64 time=0.154 ms
64 bytes from 192.168.101.2: icmp_seq=6 ttl=64 time=0.116 ms
64 bytes from 192.168.101.2: icmp_seq=7 ttl=64 time=0.105 ms
64 bytes from 192.168.101.2: icmp_seq=8 ttl=64 time=0.081 ms
```

