# VDUSE setup on Fedora 41

This documentation provides instructions to setup VDUSE on Fedora 41.

[[_TOC_]]

## Install dependencies and configure the system
### Fedora dependencies

Start with installing the minimal set of packages required to build DPDK and OVS.

``` bash
sudo dnf install -y git gcc meson pip numactl-devel autoconf automake libtool driverctl
```

### Python dependencies

pyelftools is required to build DPDK.

``` bash
pip install pyelftools
```

### Hugepages allocation

OVS-DPDK require some hugepages, let's configure it in the Kernel cmdline:

``` bash
sudo grubby --arg "default_hugepagesz=1G hugepagesz=1G hugepages=32" --update-kernel `grubby --default-kernel`
sudo reboot
```

### DPDK
#### Checkout DPDK main branch

VDUSE with re-connection support is supported since DPDK v24.11 LTS.
Let's use latest main to benefit from latest fixes and optimizations.


``` bash
cd $HOME
mkdir src
cd src
git clone git://dpdk.org/dpdk
cd dpdk
```

#### Build and install DPDK

``` bash
meson build
ninja -C build
sudo ninja -C build install
```

### OVS
#### Checkout OVS main branch

VDUSE will be supported in OVS v3.5, it is already supported in latest main.
Let's use latest main while v3.5 is released.

``` bash
cd $HOME/src
git clone https://github.com/openvswitch/ovs.git
cd ovs
```

#### Build and install OVS

``` bash
export DPDK_DIR=$HOME/src/dpdk
export DPDK_BUILD=$DPDK_DIR/build
./boot.sh
./configure --with-dpdk=static CFLAGS="-O3"
NR_CPUS=`nproc --all`
make -j$NR_CPUS
sudo make -j$NR_CPUS install
```
