# Testing Pod to Pod scenario

This test consists in creating two VDUSE devices and add them to an OVS brisge.<br>
The VDUSE devices are bound to virtio-vdpa driver.<br>
Doing that, we get two netdevs that we pass to dedicated network namespaces and ping each other.

Tuning of the system to get the best performance is covered in the end.

## Insert VDUSE and Virtio-vDPA modules dynamically

Either probe the required modules dynamically:

``` bash
sudo modprobe vduse
sudo modprobe virtio_vdpa
sudo modprobe vhost_vdpa
```

Or set it persistent across reboots:

``` bash
cat > /etc/modules-load.d/vduse.conf << EOL
vduse
virtio_vdpa
vhost_vdpa
EOL
```

## Start OVS with DPDK enabled

Currently, using VDUSE in OVS-DPDK requires root permissions.

``` bash
sudo su
export PATH=$PATH:/usr/local/bin:/usr/local/share/openvswitch/scripts
ovs-ctl --no-ovs-vswitchd start
ovs-vsctl --no-wait set Open_vSwitch . other_config:dpdk-init="true"
ovs-vsctl --no-wait set Open_vSwitch . other_config:userspace-tso-enable="true"
ovs-ctl --no-ovsdb-server start
```

## Create bridge and VDUSE devices

We create vduse0 and vduse1 ports. <br>
The distinction between vhost-user and vduse ports is done automatically by the Vhost library based on the path (/dev/vduse/).


``` bash
ovs-vsctl add-br br0 -- set bridge br0 datapath_type=netdev
ovs-vsctl add-port br0 vduse0 -- set Interface vduse0 type=dpdkvhostuserclient options:vhost-server-path=/dev/vduse/vduse0
ovs-vsctl add-port br0 vduse1 -- set Interface vduse1 type=dpdkvhostuserclient options:vhost-server-path=/dev/vduse/vduse1
```

We can check the VDUSE devices are created:

``` bash
ls -l /dev/vduse/
```
``` log
total 0
crw-------. 1 root root 235, 0 Jul 26 11:44 control
crw-------. 1 root root 235, 1 Jul 26 11:46 vduse0
crw-------. 1 root root 235, 2 Jul 26 11:46 vduse1
```

## Attach the VDUSE devices to the vDPA bus

The VDUSE devices are created, but they are not attached yet to the vDPA bus.<br>
This is done via a Netlink socket. Iproute2 provide a vdpa tool to help with that:

``` bash
vdpa dev add name vduse0 mgmtdev vduse
driverctl -b vdpa set-override vduse0 virtio_vdpa
vdpa dev add name vduse1 mgmtdev vduse
driverctl -b vdpa set-override vduse1 virtio_vdpa
```

Two new netdevs appear:

``` bash
ip l
```
``` log
11: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 56:b2:ff:50:78:45 brd ff:ff:ff:ff:ff:ff
12: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether da:9e:a6:8f:19:7b brd ff:ff:ff:ff:ff:ff
```

We can see eth0 is the netdev for vduse0 VDUSE device:

``` bash
ethtool -i eth0
```
``` log
driver: virtio_net
version: 1.0.0
firmware-version:
expansion-rom-version:
bus-info: vduse0
supports-statistics: yes
supports-test: no
supports-eeprom-access: no
supports-register-dump: no
supports-priv-flags: no
```

## Assign VDUSE netdevs to dedicated namespaces

We create two namespaces, assign one VDUSE netdev each and configures the netdevs.

For vduse0:

``` bash
ip netns add ns0
ip link set dev eth0 netns ns0
ip netns exec ns0 ip a a 192.168.101.1/24 dev eth0
ip netns exec ns0 ip l set dev eth0 up
```

And the same for vduse1:

``` bash
ip netns add ns1
ip link set dev eth1 netns ns1
ip netns exec ns1 ip a a 192.168.101.2/24 dev eth1
ip netns exec ns1 ip l set dev eth1 up
```

## Testing connectivity between namespaces

We can ping one namespace from the other to check connectivity, and use ovs-tcpdump to ensure packets are going through the VDUSE devices.

In a terminal, launch the ping:

``` bash
ip netns exec ns0 ping 192.168.101.2
```
``` log
PING 192.168.101.2 (192.168.101.2) 56(84) bytes of data.
64 bytes from 192.168.101.2: icmp_seq=1 ttl=64 time=0.279 ms
64 bytes from 192.168.101.2: icmp_seq=2 ttl=64 time=0.144 ms
64 bytes from 192.168.101.2: icmp_seq=3 ttl=64 time=0.153 ms
64 bytes from 192.168.101.2: icmp_seq=4 ttl=64 time=0.119 ms
...
```

In another terminal, dump traffic:

``` bash
sudo su
export PATH=$PATH:/usr/local/bin:/usr/local/share/openvswitch/scripts
export PYTHONPATH=$PYTHONPATH:/usr/local/share/openvswitch/python
ovs-tcpdump -i vduse0
```
``` log
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on mivduse0, link-type EN10MB (Ethernet), snapshot length 262144 bytes
12:09:27.680256 IP 192.168.101.1 > 192.168.101.2: ICMP echo request, id 28006, seq 1, length 64
12:09:27.680339 IP 192.168.101.2 > 192.168.101.1: ICMP echo reply, id 28006, seq 1, length 64
12:09:28.692927 IP 192.168.101.1 > 192.168.101.2: ICMP echo request, id 28006, seq 2, length 64
12:09:28.692982 IP 192.168.101.2 > 192.168.101.1: ICMP echo reply, id 28006, seq 2, length 64
12:09:29.717028 IP 192.168.101.1 > 192.168.101.2: ICMP echo request, id 28006, seq 3, length 64
12:09:29.717102 IP 192.168.101.2 > 192.168.101.1: ICMP echo reply, id 28006, seq 3, length 64
12:09:30.741011 IP 192.168.101.1 > 192.168.101.2: ICMP echo request, id 28006, seq 4, length 64
12:09:30.741057 IP 192.168.101.2 > 192.168.101.1: ICMP echo reply, id 28006, seq 4, length 64
...
```

## Testing reconnection

VDUSE device reconnection is an important feature, but is still WIP.<br>
While reconnection support in the VDUSE kernel module is accepted upstream, an alternative, userspace-only, implementation is added to the DPDK vduse_next branch.

In order to test it, while pinging the other namespace, we can just trigger an OVS restart:

``` bash
sudo su
export PATH=$PATH:/usr/local/bin:/usr/local/share/openvswitch/scripts
ovs-ctl restart
```
If reconnection works as expected, ping should just pause and resume once OVS is up again:

``` bash
ip netns exec ns0 ping 192.168.101.2
```
``` log
PING 192.168.101.2 (192.168.101.2) 56(84) bytes of data.
64 bytes from 192.168.101.2: icmp_seq=1 ttl=64 time=0.101 ms
64 bytes from 192.168.101.2: icmp_seq=2 ttl=64 time=0.076 ms
64 bytes from 192.168.101.2: icmp_seq=3 ttl=64 time=0.089 ms
64 bytes from 192.168.101.2: icmp_seq=5 ttl=64 time=0.367 ms # Reconnection happened here
64 bytes from 192.168.101.2: icmp_seq=6 ttl=64 time=0.108 ms
64 bytes from 192.168.101.2: icmp_seq=7 ttl=64 time=0.076 ms
^C
--- 192.168.101.2 ping statistics ---
7 packets transmitted, 6 received, 14.2857% packet loss, time 6174ms
rtt min/avg/max/mdev = 0.076/0.136/0.367/0.103 ms
```

# Tuning

This section will cover system tuning used to benchmark VDUSE with Virtio-vDPA on a
system equipped with two [Intel(R) Xeon(R) Silver 4116 CPU @ 2.10GHz](https://ark.intel.com/content/www/us/en/ark/products/120481/intel-xeon-silver-4116-processor-16-5m-cache-2-10-ghz.html).

The Intel(R) Xeon(R) Silver 4116 CPU has 12 Cores / 24 Threads, we will isolate two sibling threads for OVS PMD threads.

``` bash
lscpu | grep -E "Core|Thread|NUMA"
```
``` log
Thread(s) per core:              2
Core(s) per socket:              12
NUMA node(s):                    2
NUMA node0 CPU(s):               0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46
NUMA node1 CPU(s):               1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47
```

Let's choose CPU2 and its sibling CPU26:

``` bash
cat /sys/devices/system/cpu/cpu2/topology/thread_siblings_list
```
``` log
2,26
```

## Tuned profile

Install tuned and CPU partitioning profile:

``` bash
sudo dnf install -y tuned tuned-profiles-cpu-partitioning.noarch
```

Specifty the isolated cores in the CPU partitioning profile:

``` bash
echo "isolated_cores=2,26" | sudo tee /etc/tuned/cpu-partitioning-variables.conf
```
``` log
isolated_cores=2,26
```

Enable tuned service, select CPU partionning profile and reboot for the changes to take effects:

``` bash
sudo systemctl enable tuned
sudo systemctl start tuned
sudo tuned-adm profile cpu-partitioning
sudo reboot
```

## OVS tuning

Set CPU2 and CPU26 as PMD threads:

``` bash
sudo su
export PATH=$PATH:/usr/local/bin:/usr/local/share/openvswitch/scripts
ovs-ctl --no-ovs-vswitchd start
ovs-vsctl --no-wait set Open_vSwitch . other_config:pmd-cpu-mask="0x004000004"
ovs-ctl --no-ovsdb-server start
```

## VDUSE Virtqueues IRQ affinity

By default, softirqs will be handled on all CPUs, in a round robin manner, including NUMA1 and isolated cores on NUMA0.<br>
Set the VQ IRQ affinity to non-isolated CPUs on NUMA0 only:

``` bash
echo 5555,51555551 > /sys/class/vduse/vduse0/vq0/irq_cb_affinity
echo 5555,51555551 > /sys/class/vduse/vduse0/vq1/irq_cb_affinity
echo 5555,51555551 > /sys/class/vduse/vduse1/vq0/irq_cb_affinity
echo 5555,51555551 > /sys/class/vduse/vduse1/vq1/irq_cb_affinity
```

## Numactl

When running the benchmarks, use numactl to avoid executing and allocating memory on NUMA1 node:

``` bash
sudo dnf install -y numactl
```

Then open shells in the namespaces and, e.g., run iperf3:

``` bash
sudo numactl -m 0 -N 0 ip netns exec ns0 /bin/bash
iperf3 -s
```

``` bash
sudo numactl -m 0 -N 0 ip netns exec ns1 /bin/bash
iperf3 -f m -c 192.168.101.1 -t 10 -l 65536
```
