# Benchmarking

[[_TOC_]]

This page covers throughput and latency benchmarks with both Vhost-vDPA and Virtio-vDPA.

## Virtio-vDPA benchmarks

For Virtio-vDPA benchmarks, we compare performance between OVS-DPDK with VDUSE ports and
OVS Kernel with VETH pairs.<br>
The purpose is to assess the use of OVS-DPDK with VDUSE as primary k8s interface.

### Pod to Pod

#### Setup

The setup consists in creating two namespaces **ns0** and **ns1**.<br>
Each namespace has two interfaces:
- *Virtio*: 192.168.101.0/24 network
- *Veth*: 192.168.102.0/24 network

The Virtio interfaces are backed by VDUSE ports in a **netdev** bridge.<br>
The Veth peers are added to a standard bridge.

The tuning applied to the system is described in [setup page](/SETUP.md#tuning).

##### Create the namespaces

``` bash
sudo su
ip netns add ns0
ip netns add ns1
```

##### Create VDUSE bridge

Apply OVS configuration as described in [setup page](/SETUP.md#start-ovs-with-dpdk-enabled).<br>
Once done, you should get:

``` bash
export PATH=$PATH:/usr/local/bin:/usr/local/share/openvswitch/scripts
ovs-vsctl --no-wait get Open_vSwitch . other_config
```
``` log
{dpdk-init="true", pmd-cpu-mask="0x004000004", userspace-tso-enable="true"}
```

And follow instructions from the [setup page](/SETUP.md#create-bridge-and-vduse-devices)/<br>
Once done, you should get:

``` bash
ovs-vsctl show
```
``` log
4728bea0-e257-4b8b-ba2f-3b6edb5d27d1
    Bridge br0
        datapath_type: netdev
        Port vduse1
            Interface vduse1
                type: dpdkvhostuserclient
                options: {vhost-server-path="/dev/vduse/vduse1"}
        Port br0
            Interface br0
                type: internal
        Port vduse0
            Interface vduse0
                type: dpdkvhostuserclient
                options: {vhost-server-path="/dev/vduse/vduse0"}
    ovs_version: "3.1.90"
```

Finally assign the resulting netdevs to the namespaces and assign them IP addresses, as in the [setup page](/SETUP.md#assign-vduse-netdevs-to-dedicated-namespaces).

##### Create VETH bridge

Create two VETH pairs, assign one to each namespace and assign them IP addresses:

``` bash
ovs-vsctl add-br br1

ip link add veth0.0 type veth peer veth0.1 netns ns0
ovs-vsctl add-port br1 veth0.0
nmcli device set veth0.0 managed no
ip l set dev veth0.0 up
ip netns exec ns0 ip a a 192.168.102.1/24 dev veth0.1
ip netns exec ns0 ip l set dev veth0.1 up

ip link add veth1.0 type veth peer veth1.1 netns ns1
ovs-vsctl add-port br1 veth1.0
nmcli device set veth1.0 managed no
ip l set dev veth1.0 up
ip netns exec ns1 ip a a 192.168.102.2/24 dev veth1.1
ip netns exec ns1 ip l set dev veth1.1 up
```

Check conectivity by pinging one pod from the other:

``` bash
ip netns exec ns1 ping 192.168.102.1
```
``` log
PING 192.168.102.1 (192.168.102.1) 56(84) bytes of data.
64 bytes from 192.168.102.1: icmp_seq=1 ttl=64 time=0.548 ms
64 bytes from 192.168.102.1: icmp_seq=2 ttl=64 time=0.038 ms
64 bytes from 192.168.102.1: icmp_seq=3 ttl=64 time=0.010 ms
64 bytes from 192.168.102.1: icmp_seq=4 ttl=64 time=0.010 ms
64 bytes from 192.168.102.1: icmp_seq=5 ttl=64 time=0.010 ms
64 bytes from 192.168.102.1: icmp_seq=6 ttl=64 time=0.010 ms
64 bytes from 192.168.102.1: icmp_seq=7 ttl=64 time=0.020 ms
64 bytes from 192.168.102.1: icmp_seq=8 ttl=64 time=0.019 ms
^C
--- 192.168.102.1 ping statistics ---
8 packets transmitted, 8 received, 0% packet loss, time 7171ms
rtt min/avg/max/mdev = 0.010/0.083/0.548/0.175 ms
```

##### Install benchmarking tools

Install Iperf3 and Netperf, the later being only available in EPEL9:

``` bash
subscription-manager repos --enable codeready-builder-for-rhel-9-$(arch)-rpms
dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
dnf install -y iperf3 netperf
```

#### Iperf3 benchmark

##### Execution

Launch Iperf3 server in **ns0** with setting NUMA affinity:

```
numactl -m 0 -N 0 ip netns exec ns0 iperf3 -s
```

Run Iperf3 client on both VDUSE and Veth interfaces for different TCP buffer lengths.<br>
This can be achieved using this script:

``` bash
#! /bin/bash

len=128
duration=3

function iperf3_run()
{
	local __dst=$1
	local __len=$2
	iperf3 -f m -c $__dst -t $duration -l $__len -J 2>/dev/null | jq '.end.sum_received.bits_per_second'
}

echo "TCP Buffer size,VDUSE,VETH"
while [ $len -le 1048576 ]
do
	vduse=$(iperf3_run 192.168.101.1 $len)
	veth=$(iperf3_run 192.168.102.1 $len)
	echo "$len,$vduse,$veth"
	len=$[$len * 2]
done
```

##### Results

These benchmark results are obtained with an [Intel(R) Xeon(R) Silver 4116 CPU @ 2.10GHz](https://ark.intel.com/content/www/us/en/ark/products/120481/intel-xeon-silver-4116-processor-16-5m-cache-2-10-ghz.html) CPU.

![Iperf3 results VDUSE and VETH](/images/pod_to_pod_iperf_graph.svg)

### Pod to Physical / Physical to Pod

#### Setup

In this benchmark, the Iperf3 server is running on another machine, connected back-to-back to the DUT.<br>
One **ns0** namespace is created on the DUT, and two interfaces are attached to it:
- *Virtio*: 192.168.101.0/24 network
- *Veth*: 192.168.102.0/24 network

The Virtio interface is backed by VDUSE port in a **netdev** bridge, alongside a DPDK-driven PF.<br>
The Veth peer is added to a standard bridge, alongside a Kernel-driven PF netdev.

The tuning applied to the system is described in [setup page](/SETUP.md#tuning).

#### Iperf3 benchmark
##### Execution

The script in [Pod to Pod Iperf3 benchmark](#iperf3-benchmark) is reused and extended to assign a Kernel-driven PF and run benchmark with it as reference.

The script is executed a second time with adding iperf3's *-R* option to also benchmark the reverse direction (Physical to Pod)

##### Results

These benchmark results are obtained with an [Intel(R) Xeon(R) Silver 4116 CPU @ 2.10GHz](https://ark.intel.com/content/www/us/en/ark/products/120481/intel-xeon-silver-4116-processor-16-5m-cache-2-10-ghz.html) CPU.

![Iperf3 results Pod to Physical](/images/pod_to_physical_iperf_graph.svg)

![Iperf3 results Physical to Pod](/images/physical_to_pod_iperf_graph.svg)